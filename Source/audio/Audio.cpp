/*
 ==============================================================================
 
 Audio.cpp
 Author:  Tom Mitchell
 
 ==============================================================================
 */

#include "Audio.h"

Audio::Audio()
{
    audioDeviceManager.initialiseWithDefaultDevices (1, 2); //1 inputs, 2 outputs
    
    
    audioDeviceManager.addMidiInputCallback (String(), this);
    audioDeviceManager.addAudioCallback (this);
}

Audio::~Audio()
{
    audioDeviceManager.removeAudioCallback (this);
    audioDeviceManager.removeMidiInputCallback (String(), this);
}


void Audio::handleIncomingMidiMessage (MidiInput* source, const MidiMessage& message)
{
    //All MIDI inputs arrive here
    
}

void Audio::audioDeviceIOCallback (const float** inputChannelData,
                                   int numInputChannels,
                                   float** outputChannelData,
                                   int numOutputChannels,
                                   int numSamples)
{
    //All audio processing is done here
    const float* inL = inputChannelData[0];
    
    float *outL = outputChannelData[0];
    float *outR = outputChannelData[1];
    
    const float twoPI = 2 * M_PI;
    const float phaseIncrement = (twoPI * frequency)/sampleRate;
    phasePosition = 0;
    float fmix, fModA, fModB;
    
    const float lfoIncrement = 0.001;
    
    lfoPosition += lfoIncrement;
    if( lfoPosition > twoPI )
        lfoPosition -= twoPI;
    
    float pulseWidth = sin( lfoPosition ) * 0.5 + 0.5;
    float pulsePos = pulseWidth * 2 * M_PI; //will be automated by line to move pulse %
    
    while(numSamples--)
    {
        
        
        *outL = *inL;
        *outR = *inL;
        
        
        phasePosition = phasePosition + phaseIncrement;
        
        if(phasePosition > twoPI)
            phasePosition = phasePosition - twoPI;
        
        
         if(phasePosition < pulsePos)
             fModA = 1;
         else
             fModA = -1;
         
        fModB = sin(phasePosition);
         
        fModA = (fModA * 0.5) + (fModB * 0.3);
         
         
        fmix = fModA;
        
        
        //fmix = sin(phasePosition);
        
        *outL = fModA;
        *outR = fModA;
        
        inL++;
        
        outL++;
        outR++;
    }
}


void Audio::audioDeviceAboutToStart (AudioIODevice* device)
{
    frequency = 440.f;
    phasePosition = 0.f;
    sampleRate = device->getCurrentSampleRate();
    
}

void Audio::audioDeviceStopped()
{
    
}




